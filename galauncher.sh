#!/usr/bin/env bash

# Mandatory args
# $1: emulator binary. should be a simple name like mame, retroarch
# $2: system. Lower case, can be arcade, snes, sms etc ...
# $3: rom. fullpath or just rom name depends on the emulator
# $4: optionnal arguments. Must be transmitted to the emulator
BIN="$1"
SYS="$2"
ROM="$3"
ARGS="${@:4}"

# Useful vars for later
GAL_PATH=$(dirname "$(readlink -f $0)")
GAT_INC=/opt/gatools/include
source "$GAT_INC"/includes.sh
export GAT_LOGPATH="$MOTHER_OF_ALL"/logs


# Basic checks
# TODO...

make_action_recursive() {
  local module="$1"
  local action="$2"
  local binary="$3"
  local system="$4"
  local rom="$5"
  local other_args="${@:6}"

  for modulefile in "$GAL_PATH"/"$module"/*.sh ; do
    make_action "$modulefile" "$action" "$system" "$rom" "$other_args"
  done
}

make_action() {
  local module_file="$1"
  local action="$2"
  local system="$3"
  local rom="$4"
  local other_args="${@:5}"

  if [[ ! -f "$module_file" ]] ; then
    log_ko "Couldn't find $module_file, aborting action \"$action\""
    return 1
  fi

  ( source "$module_file" ; \
      if [[ $(type -t "$action") == "function" ]] ; then \
        filename=$(basename "$module_file")
        module_type="$(basename "$(dirname "$module_file")")"
        log_info "Starting ${module_type}/${filename}->${action}"
        "$action" "$system" "$rom" "$other_args" ; \
        return $?
      fi ; \
      log_info "${module} has no ${action}" ; \
      return 2
  )
  return $?
}

pre_launch_modules() {
  make_action_recursive "modules" "pre_launch" "$1" "$2" "$3" "${@:4}"
}

post_launch_modules() {
  make_action_recursive "modules" "post_launch" "$1" "$2" "$3" "${@:4}"
}

pre_launch_emulator() {
  make_action "$GAL_PATH"/emulators/"$1".sh pre_launch "$2" "$3" "${@:4}"
}

launch_emulator() {
  emulator="$1"
  make_action "$GAL_PATH"/emulators/"$1".sh launch "$2" "$3" "${@:4}"
}

post_launch_emulator() {
  emulator="$1"
  make_action "$GAL_PATH"/emulators/"$1".sh post_launch
}

has_to_be_launched_in_X() {
  emulator="$1"
  system="$2"
  if [[ -e "$MOTHER_OF_ALL"/configs/xonly.conf ]] ; then
    grep -qi "^${emulator} ${system}$" "$GAL_PATH"/xonly.conf "$MOTHER_OF_ALL"/configs/xonly.conf
  else
    grep -qi "^${emulator} ${system}$" "$GAL_PATH"/xonly.conf
  fi
}

# First parse options
for var in "$@"
do
  case "$var" in
    "-listxml")
      launch_emulator mame "" "-listxml"
      exit $?
      ;;
  esac
done

#If it has to be launched through X, then go through X
if has_to_be_launched_in_X "$BIN" "$SYS" && [[ -z $DISPLAY ]] ; then
  log_info "Restarting galauncher in X for $BIN $SYS"
  # Startx has a bug where it doesn't correctly expand $@
  # See https://gitlab.freedesktop.org/xorg/app/xinit/-/issues/17
  if [[ "$(head -1 $(which startx))" != '#!/bin/bash' ]] ; then
    log_info "Attempting to patch $(which startx)"
    if (cd "$(dirname "$(which startx)")" && sudo patch -Nb < "$GAL_PATH"/startx.patch) ; then
      log_ok "Succesfully patched $(which startx)"
    else
      log_ko "Patching failed, aborting"
      exit 1
    fi
  fi
  $(which startx) "$GAL_PATH"/galauncher.xinitrc "$@" -- -nocursor vt7
  exit $?
fi

pre_launch_modules "$BIN" "$SYS" "$ROM" "$ARGS"
pre_launch_emulator "$BIN" "$SYS" "$ROM" "$ARGS"
launch_emulator "$BIN" "$SYS" "$ROM" "$ARGS"
post_launch_emulator "$BIN" "$SYS" "$ROM" "$ARGS"
post_launch_modules "$BIN" "$SYS" "$ROM" "$ARGS"
