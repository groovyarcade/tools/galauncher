#!/usr/bin/env bash

pre_launch() {
  echo "Prelaunching FinalBurn Neo"
}

launch() {
  local system="$1"
  local rom="$2"

  log_info "Launching fbneo..."
  fbneo -fullscreen ${@:3} "$rom" >> "$LOG_DIR"/fbneo.log
  return $?
}

post_launch() {
  echo "Postlaunching FinalBurn Neo"
}


