#!/usr/bin/env bash

pre_launch() {
  log_info "Prelaunching mame"
}

launch() {
  local system="$1"
  local rom="$2"
  
  log_info "Launching mame..."
  log_debug "command line: groovymame "$rom" ${@:3}"
  
  if [[ "$rom" == "-listxml" ]] ; then
    groovymame "$rom" ${@:3}
  else
    groovymame "$rom" ${@:3} |& tee -a /home/arcade/shared/logs/mame.log
  fi
}

post_launch() {
  log_info "Postlaunching mame"
}
