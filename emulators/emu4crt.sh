#!/usr/bin/env bash

pre_launch() {
  log_info "Prelaunching emu4crt"
}

launch() {
  local system="$1"
  local rom="$2"

  log_info "Launching emu4crt..."
  log_debug "command line: mednafen $rom ${@:3}"

  mednafen "$rom" |& tee -a /home/arcade/shared/logs/emu4crt.log
}

post_launch() {
  log_info "Postlaunching mame"
}
