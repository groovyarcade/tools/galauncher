#!/usr/bin/env bash

pre_launch() {
  echo "Prelaunching Retroarch"
}

launch() {
  local system="$1"
  local rom="$2"

  log_info "Launching retroarch..."
  # Get the default core for the system
  core="$(get_config_value "$GA_CONF" "retroarch.${system}.core")"
  # Make sure the core exists
  core_path="$(get_ra_config_value libretro_directory)"
  core_path="$(eval echo $core_path)"
  core_file="${core_path}/${core}_libretro.so"

  assert "No core defined for system $system" test -n "$core" || return 1

  assert "Couldn,'t find '$core_file', aborting retroarch launch" test -e "$core_file" || return 1

  # Append </dev/null to prevent flycast from crashing on KMS
  # See https://github.com/libretro/flycast/issues/1118
  retroarch --verbose --log-file "$LOG_DIR"/retroarch.log -L "$core_file" "$rom" ${@:3} </dev/null
  return $?
}

post_launch() {
  echo "Postlaunching Retroarch"
}


