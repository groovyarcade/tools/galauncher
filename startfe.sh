#!/bin/bash
source /opt/gatools/include/includes.sh

launch_X() {
  #xorg_opts="-logverbose -1"
  if [[ $FE != lxde ]] ; then
    xorg_opts+=" -nocursor"
  fi
  #xorg_opts=" vt7"
  FE="$FE" startx -- $xorg_opts &>/dev/null
}

getConnectorFromKernel() {
  for p in $(</proc/cmdline) ; do
    case "$p" in
      "video"=*)
        param="${p#*=}"
        val+="${param%:*}"
        echo "$val" | cut -d ':' -f1
        return 0
        ;;
    esac
  done
  return 1
}

findConnectorCard() {
  local conn=$(getConnectorFromKernel)
  [[ -z $conn ]] && return 1
  #for card in /sys/class/drm/card? ; do
  for card in $(ls -1d /sys/class/drm/card* | grep -E "card[0-9]+$") ; do
    if [[ -d "$card"-$conn ]] ; then
      echo "/dev/dri/$(basename "$card")"
      return 0
    fi
  done
  return 1
}

launch_KMS() {
  tput civis
  case "$FE" in
    attractplus)
      log_info "Starting $FE on KMS"
      if dri_device=$(findConnectorCard) ; then
        log_info "Forcing SFML_DRM_DEVICE=$dri_device"
        export SFML_DRM_DEVICE="$dri_device"
      fi
      [[ $DEBUG == 1 ]] && fe_opts+=" --loglevel debug "
      attractplus --config "$MOTHER_OF_ALL"/frontends/attract $fe_opts &>> "$LOG_DIR"/attract.log
      ;;
    retrofe)
      log_info "Starting $FE on KMS"
      RETROFE_PATH="$MOTHER_OF_ALL"/frontends/retrofe retrofe
      ;;
    pegasus-frontend)
      log_info "Starting $FE on KMS"
      export QT_QPA_EGLFS_HIDECURSOR=1
      pegasus-fe -platform eglfs &>> "$LOG_DIR"/pegasus.log
      ;;
    groovymame)
      log_info "Starting $FE on KMS"
      groovymame &>> "$LOG_DIR"/mame.log
      ;;
    retroarch)
      log_info "Starting $FE on KMS"
      retroarch &>> "$LOG_DIR"/retroarch.log
      ;;
    emulationstation)
      log_info "Starting $FE on KMS"
      [[ $DEBUG == 1 ]] && fe_opts+=" --debug "
      emulationstation $fe_opts &>> "$LOG_DIR"/emulationstation.log
      ;;
    *)
      log_ko "Atttempted to start a non existant frontend: $FE"
      exit 1
      ;;
  esac
  rc=$?
  tput cnorm
}

video="$(get_config_value "$GA_CONF" "video.backend")"
video=${video:-X}

[[ -z $FE ]] && FE=$(get_config_value "$GA_CONF" frontend)

FE=${FE:-attract}
FE=${FE%-git}
fe_opts=""

DEBUG=$(get_config_value "$GA_CONF" verbose)

# Stop plymouth
if pgrep plymouthd &>/dev/null ; then
#  while pgrep plymouthd ; do sleep 0.01 ; done
  sudo systemctl start plymouth-quit.service
fi

# Determine running environment
if [[ $video == X || $FE == lxde ]] ; then
  launch_X
elif [[ $video == KMS ]] ; then
  launch_KMS
fi
exit $rc
