DESTDIR ?= /opt/galauncher

.PHONY: install
install:
	install -d $(DESTDIR)
	install -d $(DESTDIR)/emulators
	install -d $(DESTDIR)/modules

	install -m 755 galauncher.sh $(DESTDIR)
	install -m 755 startfe.sh $(DESTDIR)
	install -m 755 startfe-X.sh $(DESTDIR)
	install -m 755 videodata.conf $(DESTDIR)
	install -m 755 xonly.conf $(DESTDIR)
	install -m 755 galauncher.xinitrc $(DESTDIR)
	install -m 755 startx.patch $(DESTDIR)

	install -m 755 emulators/*.sh $(DESTDIR)/emulators
	install -m 755 modules/*.sh $(DESTDIR)/modules

.PHONY: uninstall
uninstall:
	rm -rf $(DESTDIR)
