#!/usr/bin/env bash
GOVERNOR_LOGFILE="$GAT_LOGPATH"/cpupower.log
DEFAULT_GOVERNOR=/tmp/cpugov

pre_launch() {
  # Store current governor
  if [[ ! -e /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor ]] ; then
    log_warn "No CPU governor available"
    return 1
  fi
  cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor > "$DEFAULT_GOVERNOR"

  # Change governor to performance if available
  if cpupower frequency-info -g | grep -q performance ; then
    log_info "Setting CPU governor to performance"
    sudo cpupower frequency-set -g performance &>> "$GOVERNOR_LOGFILE"
    restore_governor=1
  else
    log_warn "Couldn't set CPU governor to performance, the CPU doesn't support it"
  fi
}

post_launch() {
  if [[ ! -e /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor ]] ; then
    log_warn "No CPU governor available"
    return 1
  fi
  if cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor | grep -q performance ; then
    cpugov=$(cat "$DEFAULT_GOVERNOR")
    log_info "Restoring CPU governor to $cpugov"
    sudo cpupower frequency-set -g "$cpugov" &>> "$GOVERNOR_LOGFILE"
  fi
}
