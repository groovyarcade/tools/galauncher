#!/usr/bin/env bash
LEDSPICER_LOGFILE="$GAT_LOGPATH"/ledspicer.log

pre_launch() {
  local system="$1"
  local rom="$2"

  log_info  "Prelaunching ledspicer. Args : $@"
  if pgrep -x "ledspicerd" >/dev/null
  then
    emitter LoadProfileByEmulator "$rom" "$1" &>> /home/arcade/shared/logs/ledspicer.log
  else 
    log_debug " Service not running.... exiting "  
    return 1  
  fi
}

post_launch() {
  log_info "Postlaunching ledspicer. Args: $@"

  if pgrep -x "ledspicerd" >/dev/null
  then
    log_info 
    emitter FinishLastProfile &>> /home/arcade/shared/logs/ledspicer.log
  else 
    log_debug " Service not running.... exiting "  
    return 1  
  fi
}
